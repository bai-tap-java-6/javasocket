import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public Client() throws IOException {
        Scanner sc = new Scanner(System.in);
//        System.out.print("Port server: ");
//        int port = sc.nextInt();
//        sc.nextLine();
        Socket socket = new Socket("localhost", 7749);
        while(true){
            System.out.print("Client: ");
            String text = sc.nextLine();
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
            toServer.writeBytes(text + "\n");
            BufferedReader fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String mess = fromServer.readLine();
            if (mess.equals("/exit")){
                socket.close();
                break;
            }
            System.out.println("Server: " + mess);

        }
        socket.close();
    }


    public static void main(String[] args) throws IOException {
        new Client();
    }
}
