import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
    public Server() throws IOException {
        ServerSocket svSocket = new ServerSocket(7749);
        System.out.println("Port open: 7749");
        Socket socket = svSocket.accept();
        System.out.println("Ket noi Client thanh cong!");
        Scanner sc = new Scanner(System.in);
        while(true){
            BufferedReader fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String mess = fromClient.readLine();
            if(mess.equals("/exit")){
                socket.close();
                break;
            }
            System.out.println("Client: " + mess);
            DataOutputStream toClient = new DataOutputStream(socket.getOutputStream());
            System.out.print("Server: ");
            String text = sc.nextLine();
            toClient.writeBytes(text + "\n");
        }
        socket.close();


    }

    public static void main(String[] args) throws Exception {
        new Server();
    }
}
