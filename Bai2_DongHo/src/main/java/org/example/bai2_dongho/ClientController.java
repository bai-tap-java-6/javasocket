package org.example.bai2_dongho;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientController {
    @FXML
    private Label label_Time;

    public void initialize(){
        Thread getTime = new Thread(new Runnable() {
            @Override
            public void run() {
                Socket socket;
                try {
                     socket =  new Socket("localhost", 7749);
                     System.out.println("Kết nối thành công!");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                while (true){
                   String time = getTimeFromServer(socket);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    Platform.runLater(() -> label_Time.setText(time));
                }
            }
        });
        getTime.start();

    }
    public String getTimeFromServer(Socket socket){
        String time = "";
        try {
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
            BufferedReader fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            toServer.writeBytes("time" + "\n");
            time = fromServer.readLine();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return time;
    }
}