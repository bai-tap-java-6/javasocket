package org.example.bai2_dongho;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalTime;

public class Server {
    private String TimeNow;

    public String getTimeNow(){
        this.TimeNow = String.valueOf(LocalTime.now());
        int length = this.TimeNow.length();
        String tmp = "";
        for(int i = 0; i < length; i++){
            char t = this.TimeNow.charAt(i);

            if(t == '.'){
                break;
            }
            tmp = tmp + t;
        }
        this.TimeNow = tmp;
        return this.TimeNow;
    }
    Server(int a){};

    Server(){
        try {
            ServerSocket svSocket = new ServerSocket(7749);
            Socket socket = svSocket.accept();
            DataOutputStream toClient;
            BufferedReader fromClient;
            while(true){
                fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String mess = fromClient.readLine();
                if(mess.equals("time")){
                    toClient = new DataOutputStream(socket.getOutputStream());
                    String time = getTimeNow();
                    toClient.writeBytes(time + "\n");
                }else{
                    socket.close();
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static void main(String[] args) {
        new Server();
    }
}
